'use strict';

angular.module('app.login').controller('LoginController', function ($state, User) {
  var vm = this;
  vm.email = null;
  vm.password = null;
  vm.loginError = '';

  /**
   * Function for login
   */
  vm.singIn = function () {
    User.login({
      name: vm.name,
      password: vm.password
    })
      .then(function (validateError) {
        vm.loginError = validateError;
      })
      .catch(function (err) {
        vm.loginError = err.data.error.message;
      })
  };

});