"use strict";

angular.module('app.login', [])
  .config(function ($stateProvider) {
    $stateProvider
      .state('app.login', {
        url: '/login',
        params: {
          noLogin: true,
          previousState: null
        },
        component: 'app.login',
        data: {
          title: 'Login'
        },
        views: {
          "content@app": {
            controller: 'LoginController as login',
            templateUrl: "login/views/login.html"
          }
        }
      })
  });
