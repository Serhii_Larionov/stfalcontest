'use strict';

angular.module('app.login')
  .factory('User', function ($http, $state, $window) {
    /**
     * Login
     * @param user
     * @returns {*|Promise.<TResult>}
     */
    function login(user) {
      return $http.get('../api/users.json')
        .then(function (users) {
          var authorize = users.data.find(function (userData) {
            return userData.login == user.name && userData.password == user.password
          });
          if (authorize) {
            // To refresh the page
            localStorage.userName = authorize.login;
            $window.location.reload();
            $state.go('app.product', {}, {reload: true});

          }
          else {
            return "Wrong password or name";
          }
        });
    }

    /**
     * Logout
     */
    function logOut() {
      delete localStorage.userName;
      $state.go('app.login', {}, {reload: true});
    }

    return {
      login: login,
      logOut: logOut
    }
  });