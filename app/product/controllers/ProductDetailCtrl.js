'use strict';

angular.module('app.product').controller('ProductDetailController', function ($scope, $uibModalInstance, details) {
  var vm = this;
  vm.details = details;

  /**
   * Close modal
   */
  $scope.ok = function () {
    $uibModalInstance.close();
  };
});