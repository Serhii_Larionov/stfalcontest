'use strict';

angular.module('app.product').controller('ProductController', function ($scope, $state, $uibModal, Product) {
  var vm = this;
  vm.email = null;
  vm.password = null;
  vm.loginError = '';
  vm.products = [];

  Product.get()
    .then(function (products) {
      vm.products = products;
    })
    .catch(function (err) {
      console.log(err);
    });

  /**
   * Function for displaying modal with product details
   * @param index
   */
  $scope.showModal = function (index) {
    $uibModal.open({
      templateUrl: 'product/templates/productDetail.html',
      controller: 'ProductDetailController as productDetailCtrl',
      resolve: {
        details: function (Product) {
          return Product.getDetail(vm.products[index].id)
            .then(function (details) {
              return details;
            })
            .catch(function (err) {
              console.log(err);
              return {};
            });
        }
      }
    })
      .result
      .then(
        function () {
          alert("OK");
        });
  }
});