"use strict";

angular.module('app.product', ['ui.router', 'ui.bootstrap'])
  .config(function ($stateProvider) {
    $stateProvider
      .state('app.product', {
        url: '/products',
        data: {
          title: 'Products'
        },
        views: {
          "content@app": {
            controller: 'ProductController as productCtrl',
            templateUrl: "product/templates/product.html"
          }
        }
      });
  });

