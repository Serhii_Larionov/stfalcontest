'use strict';

angular.module('app.product')
  .factory('Product', function ($http, $state) {
    /**
     * Function for getting products list
     * @returns {*|Promise.<TResult>}
     */
    function get() {
      return $http.get('../api/products.json')
        .then(function (products) {
          return products.data;
        });
    }

    /**
     * Function for getting details list by product id
     * @param productId
     * @returns {*|Promise.<TResult>}
     */
    function getDetail(productId) {
      return $http.get('../api/productDetails.json')
        .then(function (productDetails) {
          var detail = productDetails.data.find(function (productDetail) {
            return (productDetail.productId === productId);
          });
          return detail;
        });
    }

    return {
      get: get,
      getDetail: getDetail
    }
  });