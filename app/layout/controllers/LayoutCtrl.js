'use strict';

angular.module('app.layout').controller('LayoutController', function ($scope, User) {
  var vm = this;
  vm.userName = null;

  /**
   * Updating userName after login/logout
   */
  $scope.$watch('localStorage.userName', function () {
    vm.userName = localStorage.userName;
  });

  /**
   * Function for logout
   */
  vm.logOut = function () {
    delete vm.userName;
    User.logOut();
  };
});