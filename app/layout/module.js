"use strict";

angular.module('app.layout', ['ui.router'])
  .config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('app', {
        views: {
          root: {
            templateUrl: 'layout/templates/layout.html',
            controller: 'LayoutController as layout'
          }
        }
      });
    $urlRouterProvider.otherwise('/');
  });

