'use strict';

// Declare app level module which depends on views, and components
angular.module('app', [
  'ui.router',
  'ui.bootstrap',

  // App
  'app.login',
  'app.layout',
  'app.product'
])
  .run(function ($rootScope, $state, $stateParams, User) {
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;

    $rootScope.$on('$stateChangeSuccess',
      function (event, toState, toParams) {
        if (!toParams.noLogin) {
          if (!localStorage.userName) {
            $state.go('app.login', {}, {reload: true});
          }
        }
      }
    );
  });